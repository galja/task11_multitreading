package com.epam;

public class PingPong extends Thread {
    private final String phrase;
    private int i = 0;
    private Main main;

    public PingPong(Main main, String phrase) {
        this.phrase = phrase;
        this.main = main;
    }

    @Override
    public void run() {
        int i = 0;
        while (true) {
            serve();
            i++;
        }
    }

    public void serve() {
        synchronized (main) {

            while (i < 100) {
                i++;
                try {
                    System.out.println(phrase);
                    main.notify();
                    main.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            main.notify();
        }
    }
}

class Main {
    public static void main(String[] args) {
        Main m= new Main();
        m.st();
    }

    public void st(){
        PingPong ping = new PingPong(this,"Ping");
        PingPong pong = new PingPong(this, "Pong");
        ping.start();
        pong.start();
    }
}